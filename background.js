var browser = browser || chrome;
browser.runtime.onMessage.addListener(dl);

function dl(msg) {
    let downloading = browser.downloads.download({
        url: msg.url,
        filename: msg.filename
    });
}

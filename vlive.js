var browser = browser || chrome;
function XHR() {
    try {
      return XPCNativeWrapper(new window.wrappedJSObject.XMLHttpRequest());
   }
   catch(evt){
      return new XMLHttpRequest();
   }
}

function download(url, filename) {
    browser.runtime.sendMessage({"url": url, "filename": filename});
}

function createBox() {
    let box = document.createElement('div');
    box.setAttribute('class', 'download-box');

    return box;
}

function createButton(text, list) {
    let button = document.createElement('div');
    button.setAttribute('class', 'download-button');
    button.textContent = text;
    button.innerHTML += '<svg class="arrow" width="12" height="12" viewBox="0 0 12 12"><g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g stroke="#979797"><g><g><path d="M.455.455L5 5M9.545.455L5 5" transform="translate(-209 -1709) translate(209 1709) translate(1 3)"></path></g></g></g></g></svg>';

    button.addEventListener("click", function(e) {
        if (list.style.display == "none") {
            list.style.display = "block";
        } else {
            list.style.display = "none";
        }
    });


    return button;
}

function createList() {
    let list = document.createElement('div');
    list.setAttribute('class', 'download-list');
    list.style.display = "none";

    return list;
}

function createItem(text, url, filename) {
    let item = document.createElement('a');
    item.textContent = text;
    item.setAttribute('class', 'download-item');
    item.setAttribute('href', url);

    item.addEventListener("click", function(e) {
        e.preventDefault();
        download(url, filename.replace(/\/|:|\?|\*|\||\\|"|<|>/g, "_"));
    });

    return item;
}

function addDownloadLinks(vodId, key, title, videoSeq) {
    let getVideo = XHR();
    getVideo.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let r = JSON.parse(this.responseText);
            let download_box;
            let download_button;
            let download_list;

            if (r.videos !== undefined && r.videos.list.length > 0 && !document.querySelector("#download-video-box")) {
                download_box = createBox();
                download_box.setAttribute('id', 'download-video-box');

                download_list = createList();

                download_button = createButton("Download video", download_list);

                r.videos.list.forEach(function(v) {
                    let div = createItem(v.encodingOption.name, v.source, title+"-"+videoSeq+".mp4");

                    download_list.appendChild(div);
                });

                download_box.appendChild(download_button);
                download_box.appendChild(download_list);
            }

            let subtitles_box;
            let subtitles_button;
            let subtitles_list;

            if (r.captions !== undefined && r.captions.list.length > 0 && !document.querySelector("#download-subs-box")) {
                subtitles_box = createBox();
                subtitles_box.setAttribute('id', 'download-subs-box');

                subtitles_list = createList();

                subtitles_button = createButton("Download subtitles", subtitles_list);

                r.captions.list.forEach(function(s) {
                    if (s.type != "auto") {
                        let label = s.label;
                        if (s.subLabel !== undefined) {
                            label += " ("+s.subLabel+")";
                        }

                        let div = createItem(label, s.source, title+"-"+videoSeq+"."+s.locale+".vtt");

                        subtitles_list.appendChild(div);
                    }
                });

                subtitles_box.appendChild(subtitles_button);
                subtitles_box.appendChild(subtitles_list);
            }

            let video_detail = document.querySelector("div[class|=video_detail");
            let player_area = document.querySelector("div[class|=detail_content_wrap]");

            if (download_box) {
                video_detail.insertBefore(download_box, player_area);
                download_list.style.width = getComputedStyle(download_button).width;
            }
            if (subtitles_box) {
                video_detail.insertBefore(subtitles_box, player_area);
                subtitles_list.style.width = getComputedStyle(subtitles_button).width;
            }
        }
    };
    getVideo.open("GET", "https://apis.naver.com/rmcnmv/rmcnmv/vod/play/v2.0/"+vodId+"?key="+key, true);
    getVideo.send();
}

function doVideo(videoSeq, vodId, title) {
    let getKey = XHR();
    getKey.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let r = JSON.parse(this.responseText);
            addDownloadLinks(vodId, r.inkey, title, videoSeq);
        }
    };
	let url = "https://www.vlive.tv/globalv-web/vam-web/video/v1.0/vod/"+videoSeq+"/inkey?appId=8c6cc7b45d2568fb668be6e05b6e5a3b&platformType=PC";
	if (localStorage.vpdid2 !== undefined) {
		url += "&vpdid2="+localStorage.vpdid2;
	}
	getKey.open("GET", url, true);
    getKey.send();
}

function doPost(postid) {
    let getPost = XHR(); 
    getPost.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let r = JSON.parse(this.responseText);
            
            if (r.officialVideo) {
                let title = r.officialVideo.title;

                if (r.officialVideo.multinationalTitles !== undefined) {
                    for (const t of r.officialVideo.multinationalTitles) {
                        if (t.type = "VIDEO_TITLE" && t.locale == "en_US") {
                            title = t.label;
                            break;
                        }
                    }
                }

                doVideo(r.officialVideo.videoSeq, r.officialVideo.vodId, title);
            }
        }
    };
    getPost.open("GET", "https://www.vlive.tv/globalv-web/vam-web/post/v1.0/post-"+postid+"?appId=8c6cc7b45d2568fb668be6e05b6e5a3b&fields=attachments,officialVideo,title,originPost", true);
    getPost.send();
}

function doVideoPost(videoid) {
    let getPost = XHR();
    getPost.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let r = JSON.parse(this.responseText);

            if (r.officialVideo) {
                let title = r.officialVideo.title;

                if (r.officialVideo.multinationalTitles !== undefined) {
                    for (const t of r.officialVideo.multinationalTitles) {
                        if (t.type = "VIDEO_TITLE" && t.locale == "en_US") {
                            title = t.label;
                            break;
                        }
                    }
                }
            
                doVideo(r.officialVideo.videoSeq, r.officialVideo.vodId, title);
            }
        }
    };
    getPost.open("GET", "https://www.vlive.tv/globalv-web/vam-web/post/v1.0/officialVideoPost-"+videoid+"?appId=8c6cc7b45d2568fb668be6e05b6e5a3b&fields=attachments,officialVideo,title,originPost", true);
    getPost.send();
}

function checkDone() {
    let postHeader = document.querySelector("div[class|=post_header]");
    if (!postHeader || postHeader.getAttribute('x-done') == '1') {
        return true;
    }
    postHeader.setAttribute('x-done', '1');
    return false;
}

let ob = new window.MutationObserver(function() {
    let postid = /^https:\/\/www\.vlive\.tv\/post\/(\d+-\d+).*$/.exec(window.location.href);
    if (postid) {
        if (!checkDone()) {
            doPost(postid[1]);
        }
        return;
    }
    let videoid = /^https:\/\/www\.vlive\.tv\/video\/(\d+).*$/.exec(window.location.href);
    if (videoid) {
        if (!checkDone()) {
            doVideoPost(videoid[1]);
        }
        return;
    }
});

let root = document.querySelector("div#root");

if (root) {
    ob.observe(document.querySelector("div#root"), {
        childList: true,
        subtree: true,
    });
}

window.addEventListener('click', function(e) {
    if (!e.target.matches('.download-button') && !e.target.matches('.download-item') && !e.target.matches('.download-list')) {
        let lists = document.querySelectorAll('.download-list');
        lists.forEach(function(l) {
            if (l.style.display != "none") {
                l.style.display = "none";
            }
        });
    }
});

